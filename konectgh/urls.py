from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'lodging.views.index', name='index'),
    url(r'^index/$', 'lodging.views.index', name='index'),
    url(r'^about/$', 'lodging.views.about', name='about'),
    url(r'^booking/$', 'lodging.views.booking', name='booking'),
    url(r'^ashanti/$', 'lodging.views.ashanti', name='ashanti'),
    url(r'^brong-ahafo/$', 'lodging.views.brong_ahafo', name='brong-ahafo'),
    url(r'^car-rental/$', 'lodging.views.car_rental', name='car-rental'),
    url(r'^central/$', 'lodging.views.central', name='central'),
    url(r'^eastern/$', 'lodging.views.eastern', name='eastern'),
    url(r'^contact/$', 'lodging.views.contact', name='contact'),
    url(r'^excursion/$', 'lodging.views.excursion', name='excursion'),
    url(r'^flight-reservation/$', 'lodging.views.flight_reservation', name='flight-reservation'),
    url(r'^ghana/$', 'lodging.views.ghana', name='ghana'),
    url(r'^greater-accra/$', 'lodging.views.greater_accra', name='greater-accra'),
    url(r'^lodging/$', 'lodging.views.lodging', name='lodging'),
    url(r'^northern/$', 'lodging.views.northern', name='northern'),
    url(r'^services/$', 'lodging.views.services', name='services'),
    url(r'^volta/$', 'lodging.views.volta', name='volta'),
    url(r'^western/$', 'lodging.views.western', name='western'),
    url(r'^admin/', include(admin.site.urls)),
)
