from django.contrib import admin
from lodging.models import Tour, Interest, BookingFrom

class TourAdmin(admin.ModelAdmin):
        search_fields = ['first_name', 'last_name']
        list_display = ['first_name', 'last_name', 'country', 'phone_number', 'email', 'no_of_people']

admin.site.register(Tour, TourAdmin)
admin.site.register(Interest)
admin.site.register(BookingFrom)
