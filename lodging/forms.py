from django import forms
from lodging.models import Tour, Interest, BookingFrom

class TourForm(forms.ModelForm):
        interests = forms.ModelMultipleChoiceField(queryset=Interest.objects.all(), required=False, widget=forms.CheckboxSelectMultiple)
        booking_froms = forms.ModelMultipleChoiceField(queryset=BookingFrom.objects.all(), required=False, widget=forms.RadioSelect)
        class Meta:
                model = Tour
