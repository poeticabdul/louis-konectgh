# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BookingFrom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('phone_number', models.CharField(max_length=20)),
                ('address', models.CharField(max_length=200)),
                ('city', models.CharField(max_length=50)),
                ('state', models.CharField(max_length=50)),
                ('zip_code', models.CharField(max_length=50)),
                ('country', models.CharField(max_length=100)),
                ('tour_package', models.CharField(max_length=50, choices=[(b'Forts, Castles and Enhancing Beaches', b'Forts, Castles and Enhancing Beaches'), (b'The Golden Triangle Tour', b'The Golden Triangle Tour'), (b'Wonderful West', b'Wonderful West'), (b'Ashanti Kingdom and the East', b'Ashanti Kingdom and the East'), (b'Adventurous Northern Ghana', b'Adventurous Northern Ghana'), (b'The Alluring Volta', b'The Alluring Volta')])),
                ('accommodation_type', models.CharField(max_length=50, choices=[(b'Guest House', b'Guest House'), (b'Hotel', b'Hotel'), (b'Motel', b'Motel'), (b'Hostel', b'Hostel'), (b'Stay with African Family', b'Stay with African Family')])),
                ('other_information', models.TextField(null=True, blank=True)),
                ('who_is_coming', models.CharField(max_length=50, choices=[(b'Alone', b'Alone'), (b'With Kids', b'With Kids'), (b'With Family', b'With Family'), (b'With a Group', b'With a Group')])),
                ('no_of_people', models.IntegerField()),
                ('no_of_kids', models.IntegerField()),
                ('added_file', models.FileField(null=True, upload_to=b'', blank=True)),
                ('comment', models.TextField(null=True, blank=True)),
                ('booking_from', models.ManyToManyField(to='lodging.BookingFrom')),
                ('interests', models.ManyToManyField(to='lodging.Interest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
