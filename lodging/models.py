from django.db import models

class Interest(models.Model):
        text = models.CharField(max_length=300)

        def __unicode__(self):
          return self.text;

class BookingFrom(models.Model):
        text = models.CharField(max_length=300)

        def __unicode__(self):
          return self.text;

class Tour(models.Model):

        TOUR_PACKAGE = (
               ('Forts, Castles and Enhancing Beaches', 'Forts, Castles and Enhancing Beaches'),
               ('The Golden Triangle Tour', 'The Golden Triangle Tour'),
               ('Wonderful West', 'Wonderful West'),
               ('Ashanti Kingdom and the East', 'Ashanti Kingdom and the East'),
               ('Adventurous Northern Ghana', 'Adventurous Northern Ghana'),
               ('The Alluring Volta', 'The Alluring Volta'),
        )
        ACCOMMODATION = (
               ('Guest House', 'Guest House'),
               ('Hotel', 'Hotel'),
               ('Motel', 'Motel'),
               ('Hostel', 'Hostel'),
               ('Stay with African Family', 'Stay with African Family'),
        )
        WHO_IS_COMING = (
               ('Alone', 'Alone'),
               ('With Kids', 'With Kids'),
               ('With Family', 'With Family'),
               ('With a Group', 'With a Group'),
        )
        first_name = models.CharField(max_length=200)
        last_name = models.CharField(max_length=200)
        email = models.EmailField(unique=True)
        phone_number = models.CharField(max_length=20)
        address = models.CharField(max_length=200)
        city = models.CharField(max_length=50)
        state = models.CharField(max_length=50)
        zip_code = models.CharField(max_length=50)
        country = models.CharField(max_length=100)
        tour_package = models.CharField(max_length=50, choices=TOUR_PACKAGE)
        accommodation_type = models.CharField(max_length=50, choices=ACCOMMODATION)
        other_information = models.TextField(blank=True, null=True)
        interests = models.ManyToManyField(Interest)
        booking_from = models.ManyToManyField(BookingFrom)
        who_is_coming = models.CharField(max_length=50, choices=WHO_IS_COMING)
        no_of_people = models.IntegerField()
        no_of_kids = models.IntegerField()
        added_file = models.FileField(blank=True, null=True)
        comment = models.TextField(blank=True, null=True)
















