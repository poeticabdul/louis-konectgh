from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render_to_response
from lodging.forms import TourForm

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def ashanti(request):
    return render(request, 'ashanti.html')

def booking(request):
    return render(request, 'booking.html')

def brong_ahafo(request):
    return render(request, 'brong_ahafo.html')

def car_rental(request):
    return render(request, 'car_rental.html')

def central(request):
    return render(request, 'central.html')

def contact(request):
    return render(request, 'contact.html')

def eastern(request):
    return render(request, 'eastern.html')

def excursion(request):
    return render(request, 'excursion.html')

def flight_reservation(request):
    return render(request, 'flight_reservation.html')

def ghana(request):
    return render(request, 'ghana.html')

def greater_accra(request):
    return render(request, 'greater_accra.html')

def lodging(request):
    return render(request, 'lodging.html')

def booking(request):
    if request.method=='POST':
        form =  TourForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = TourForm()

    return render_to_response('booking.html', {"form": form}, context_instance=RequestContext(request))

def northern(request):
    return render(request, 'northern.html')

def services(request):
    return render(request, 'services.html')

def volta(request):
    return render(request, 'volta.html')

def western(request):
    return render(request, 'western.html')
